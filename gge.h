///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

const double GASGAL_IN_A_JOULE         = ( 1 / 1.213e8 );
const char GASGAL                      = 'g';

extern double fromGasGalToJoule( double gasgal) ;

extern double fromJouleToGasGal( double joule) ;
