///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGasGalToJoule( double gasgal) {
   return gasgal / GASGAL_IN_A_JOULE;
}


double fromJouleToGasGal( double joule) {
   return joule * GASGAL_IN_A_JOULE;
}

